		</div>
	    <div id="footer">
	        <div>
	            <p>Copyright &copy; 2008-2009 StoneOne AG. All rights reserved.</p>
	            <p>Powered by <a href="http://www.ja-sig.org/products/cas/">JA-SIG Central Authentication Service <%=org.jasig.cas.CasVersion.getVersion()%></a></p>
	        </div>
	        <a href="http://www.stoneone.de" title="go to JA-SIG home page"><img id="logo" src="images/s1-logo.jpg" width="130" height="59" alt="StoneOne AG" title="go to StoneOne AG home" /></a>
	    </div>
	</body>
</html>
