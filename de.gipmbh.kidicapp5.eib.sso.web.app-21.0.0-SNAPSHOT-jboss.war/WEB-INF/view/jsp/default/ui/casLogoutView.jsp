<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="false" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta http-equiv="content-type" content="text/html; charset=UTF-8">
   <title><spring:message code="application.title" /></title>
   <meta http-equiv="expires" content="0">   
   <meta http-equiv="cache-control" content="no-cache, no-store, max-age=0, must-revalidate">
   <meta http-equiv="pragma" content="no-cache">   
   <script src="js/framework.js" type="text/javascript"></script>
   <script src="js/ui.js" type="text/javascript"></script>
   <link class="component" href="css/eakte_002.css" rel="stylesheet" type="text/css">
   <link class="component" href="css/eakte_005.css" rel="stylesheet" type="text/css">
   <link class="component" href="css/eakte_003.css" rel="stylesheet" type="text/css">
   <link class="component" href="css/eakte_004.css" rel="stylesheet" type="text/css">
   <link class="component" href="css/eakte_001.css" rel="stylesheet" type="text/css">
   <script type="text/javascript">window.RICH_FACES_EXTENDED_SKINNING_ON=true;</script>
   <script src="js/functions.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/common_rosters.js"></script>
   <link href="images/gip.ico" rel="SHORTCUT ICON" type="image/x-icon">
</head>

<body id="cas" onload="init();">

   <div class="wrapper">

      <div class="loginHeader">
         <img id="logo" src="images/logo.png">
         <p id="logoCaption">leichter mitarbeiten in der Personalwirtschaft</p>
         <div class="horizontalLine"></div>
      </div>

      <div id="middle">
         <div id="left-col">
            <h2><spring:message code="application.title" /></h2>
            <span class="fotos"></span>
            <span class="verticalLine"></span>
         </div>
         <div id="right-col">
            <div class="inputData">

               <div class="loginFormHeader">
                  <h2><spring:message code="screen.welcome.label.logout" /></h2>
               </div>

               <div class="loginInputBox">
                  <spring:message code="screen.logout.header" />.
               </div>
                  
               <div class="loginInputBox" style="font-size: 1.1em; margin-top: 20px;">
                  <c:if test="${empty param['url']}">
                  <a href="<%= request.getContextPath() %>/login?service=%2Fde.gipmbh.kidicapp5.eakte%2F"><spring:message code="screen.logout.redirect.label" /></a>
                  </c:if>

                  <c:if test="${not empty param['url']}">
                  <spring:message code="screen.logout.redirect" arguments="${fn:escapeXml(param.url)}" />
                  </c:if>
               </div>

            </div>
         </div>

      </div>

      <div class="footer">
         <div class="versionInfo">Version 14.5 (build 0815)</div>
         <div class="horizontalLine"></div>
         <div class="footerImage"></div>
      </div>

   </div>

</body>
</html>
