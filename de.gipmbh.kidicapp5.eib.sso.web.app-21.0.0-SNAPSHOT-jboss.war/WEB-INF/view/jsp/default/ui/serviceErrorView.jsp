<jsp:directive.include file="includes/top_s1.jsp" />
		<div id="status" class="errors">
			<h2><spring:message code="screen.service.error.header" /></h2>
			<p><spring:message code="screen.service.error.message" /></p>
		</div>
<jsp:directive.include file="includes/bottom_s1.jsp" />
