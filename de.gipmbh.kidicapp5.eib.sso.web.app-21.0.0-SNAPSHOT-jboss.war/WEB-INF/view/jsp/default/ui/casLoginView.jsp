<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="true" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta http-equiv="content-type" content="text/html; charset=UTF-8">
   <title><spring:message code="application.title" /></title>
   <meta http-equiv="expires" content="0">   
   <meta http-equiv="cache-control" content="no-cache, no-store, max-age=0, must-revalidate">
   <meta http-equiv="pragma" content="no-cache">   
   <script src="js/framework.js" type="text/javascript"></script>
   <script src="js/ui.js" type="text/javascript"></script>
   <link class="component" href="css/eakte_002.css" rel="stylesheet" type="text/css">
   <link class="component" href="css/eakte_005.css" rel="stylesheet" type="text/css">
   <link class="component" href="css/eakte_003.css" rel="stylesheet" type="text/css">
   <link class="component" href="css/eakte_004.css" rel="stylesheet" type="text/css">
   <link class="component" href="css/eakte_001.css" rel="stylesheet" type="text/css">
   <script type="text/javascript">window.RICH_FACES_EXTENDED_SKINNING_ON=true;</script>
   <script src="js/functions.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/common_rosters.js"></script>
   <link href="images/gip.ico" rel="SHORTCUT ICON" type="image/x-icon">
</head>

<body id="cas" onload="focusElement('loginForm:username');">

   <%-- test comment --%>

   <div class="wrapper">
      <div class="loginHeader">
         <img id="logo" src="images/logo.png">
         <p id="logoCaption">leichter mitarbeiten in der Personalwirtschaft</p>
         <div class="horizontalLine"></div>
      </div>
      <div id="middle">
         <div id="left-col">
            <h2><spring:message code="application.title" /></h2>
            <span class="fotos"></span>
            <span class="verticalLine"></span>
         </div>
         <div id="right-col">
            <div class="inputData">
               <form:form method="post" id="loginForm" commandName="${commandName}" htmlEscape="true">
               <!-- form id="loginForm" name="loginForm" method="post" action="TODO" enctype="application/x-www-form-urlencoded" -->

                  <input name="loginForm" value="loginForm" type="hidden">
   
                  <div class="loginFormHeader">
                     <span id="_viewRoot:status">
                        <span id="_viewRoot:status.start" style="display: none">
                           <span class="statusSpinner"></span>
                        </span>
                        <span id="_viewRoot:status.stop"></span>
                     </span>
                     <h2><spring:message code="screen.welcome.label.login" /></h2>
                  </div>

                  <div class="loginFooter">
                     <table class="loginFormElements" cellspacing="0">
                        <tbody>
                           <tr>
                              <td class="loginFormLabel">
                                 <label for="loginForm:username">
                                    <spring:message code="screen.welcome.label.netid.s1" />
                                 </label>
                              </td>
                              <td class="loginFormInput">
   
                                 <c:if test="${not empty sessionScope.openIdLocalId}">
                                 <strong>${sessionScope.openIdLocalId}</strong>
                                 <input type="hidden" id="loginForm:username" name="username" value="${sessionScope.openIdLocalId}" />
                                 </c:if>
                                 
                                 <c:if test="${empty sessionScope.openIdLocalId}">
                                 <spring:message code="screen.welcome.label.netid.accesskey" var="userNameAccessKey" />
                                 <form:input
                                    cssClass="loginFormInputField"
                                    cssErrorClass="loginFormInputFieldError"
                                    id="loginForm:username"
                                    name="username"
                                    title="Nutzerkennung"
                                    size="25"
                                    tabindex="1"
                                    accesskey="${userNameAccessKey}"
                                    path="username"
                                    autocomplete="false"
                                    htmlEscape="true" />
                                 </c:if>
   
                              </td>
                           </tr>
   
                           <tr>
                              <td class="loginFormLabel">
                                 <label for="loginForm:password">
                                    <spring:message code="screen.welcome.label.password" />
                                 </label>
                              </td>
                              <td class="loginFormInput">
                                 <spring:message code="screen.welcome.label.password.accesskey" var="passwordAccessKey" />
                                 <form:password
                                    cssClass="loginFormInputField"
                                    cssErrorClass="loginFormInputFieldError"
                                    id="loginForm:password"
                                    name="password"
                                    title="Passwort"
                                    size="25"
                                    tabindex="2"
                                    path="password"
                                    accesskey="${passwordAccessKey}"
                                    htmlEscape="true"
                                    autocomplete="off" />
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>

                  <form:errors path="*" cssClass="errors rich-messages" id="loginStatus" element="div" />

                  <dl id="loginForm:errorList" class="rich-messages" style="display: none; ">
                     <dt></dt>
                  </dl>

                  <div class="loginFooter">
<!--
                     <a href="#" id="loginForm:forgotPassword" name="loginForm:forgotPassword" onclick="A4J.AJAX.Submit('loginForm',event,{'similarityGroupingId':'loginForm:forgotPassword','parameters':{'loginForm:forgotPassword':'loginForm:forgotPassword'} } );return false;" title="Passwort vergessen">
                        Passwort vergessen?
                     </a>
-->
                     <input id="loginForm:loginButton" name="submit" value="<spring:message code="screen.welcome.button.login" />" title="<spring:message code="screen.welcome.button.login" />" tabindex="3" type="submit" />

                  </div>

                  <input type="hidden" name="lt" value="${flowExecutionKey}" />
                  <input type="hidden" name="_eventId" value="submit" />

                  <input id="warn" name="warn" value="false" type="hidden" />
               </form:form>
            </div>
         </div>

      </div>

      <div class="footer">
         <div class="versionInfo">Version 14.5 (build 0815)</div>
         <div class="horizontalLine"></div>
         <div class="footerImage"></div>
      </div>

   </div>

</body>
</html>
