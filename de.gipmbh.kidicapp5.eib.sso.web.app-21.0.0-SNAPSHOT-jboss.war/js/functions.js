
function focusElement(_elementId){if(_elementId==null){return;}
var element=document.getElementById(_elementId);if(element==null){return;}
element.focus();if(element.select){element.select();}}
function autofocus(containerId){var element=jQuery(":input:not(:button):visible:enabled:first","#"+containerId);if(element!=null){element.focus().select();}}
function selectRow(row,styleClass){jQuery("."+styleClass).removeClass(styleClass);jQuery(row).addClass(styleClass);}
function autoCompleteDate(dateField,locale){if(dateField==null||locale==null){return;}
switch(locale){case'de':autoCompleteDate_de(dateField);break;case'en':break;default:break;}}
function autoCompleteDate_de(dateField){if(dateField==null){return;}
var date=dateField.value;if(date==""){return;}
var newdate="";var arr=new Array(/\d\/\d+\/\d/,/\d\;\d+\;\d/,/\d\,\d+\,\d/,/\d\*\d+\*\d/,/\d\.\d+\.\d/,/\d\-\d+\-\d/,/\d\:\d+\:\d/);var arr2=new Array("/",";",",","*",".","-",":");var zeichen=date.length;var now=new Date();if(date.match(/^\d+$/)){switch(zeichen){case 1:var tag_1="0"+date;var monat_1=now.getMonth()+1;var jahr_1=now.getFullYear();if(monat_1<10){monat_1="0"+monat_1;}
newdate=tag_1+"."+monat_1+"."+jahr_1;break;case 2:var tag_2=date;var monat_2=now.getMonth()+1;var jahr_2=now.getFullYear();if(monat_2<10){monat_2="0"+monat_2;}
if(tag_2>31){tag_2="0"+date.substr(0,1);monat_2="0"+date.substr(1);}
newdate=tag_2+"."+monat_2+"."+jahr_2;break;case 3:var tag_3=date.substr(0,2);var monat_3="0"+date.substr(2);var jahr_3=now.getFullYear();if(tag_3>31){tag_3="0"+date.substr(0,1);monat_3="0"+date.substr(1,1);jahr_3="200"+date.substr(2);}
newdate=tag_3+"."+monat_3+"."+jahr_3;break;case 4:var tag_4=date.substr(0,2);var monat_4=date.substr(2);var jahr_4=now.getFullYear();if(tag_4>31){tag_4="0"+date.substr(0,1);monat_4="0"+date.substr(1,1);jahr_4="20"+date.substr(2);}
if(monat_4>12){monat_4="0"+date.substr(2,1);jahr_4="200"+date.substr(3);}
newdate=tag_4+"."+monat_4+"."+jahr_4;break;case 5:var tag_5=date.substr(0,2);var monat_5=date.substr(2,2);var jahr_5="200"+date.substr(4);if(tag_5>31){tag_5="0"+date.substr(0,1);monat_5="0"+date.substr(1,1);jahr_5="2"+date.substr(2);}
if(monat_5>12){monat_5="0"+date.substr(2,1);jahr_5="20"+date.substr(3);}
newdate=tag_5+"."+monat_5+"."+jahr_5;break;case 6:var tag_6=date.substr(0,2);var monat_6=date.substr(2,2);var jahr_6="20"+date.substr(4,2);newdate=tag_6+"."+monat_6+"."+jahr_6;break;case 8:var tag_8=date.substr(0,2);var monat_8=date.substr(2,2);var jahr_8=date.substr(4,4);newdate=tag_8+"."+monat_8+"."+jahr_8;break;default:break;}}
else{for(i=0;i<arr.length;i++){if(date.match(arr[i])){date=date.split(arr2[i]);var tag=date[0];var monat=date[1];var jahr=date[2];dateField.value=tag+""+monat+""+jahr;autoCompleteDate_de(dateField);return;}}}
dateField.value=newdate;}
function autoCompleteTime(timeField,locale){if(timeField==null||locale==null){return;}
switch(locale){case'de':autoCompleteTime_de(timeField);break;case'en':break;default:break;}}
function autoCompleteTime_de(timeField){if(timeField==null){return;}
var time=timeField.value;if(time==""){return;}
var newtime="";var arr=new Array(/\d\/\d/,/\d\;\d/,/\d\,\d/,/\d\*\d/,/\d\.\d/,/\d\-\d/,/\d\:\d/);var arr2=new Array("/",";",",","*",".","-",":");var zeichen=time.length;if(time.match(/^\d+$/)){switch(zeichen){case 1:var stunde_1="0"+time;var minute_1="00";newtime=stunde_1+":"+minute_1;break;case 2:var stunde_2=time;var minute_2="00";if(stunde_2>23){stunde_2="0"+time.substr(0,1);minute_2="0"+time.substr(1);}
newtime=stunde_2+":"+minute_2;break;case 3:var stunde_3=time.substr(0,2);var minute_3="0"+time.substr(2);if(stunde_3>23){stunde_3="0"+time.substr(0,1);minute_3=time.substr(1);}
newtime=stunde_3+":"+minute_3;break;case 4:var stunde_4=time.substr(0,2);var minute_4=time.substr(2);if(stunde_4>23){break;}
newtime=stunde_4+":"+minute_4;break;default:break;}}
else{for(i=0;i<arr.length;i++){if(time.match(arr[i])){time=time.split(arr2[i]);var stunde=time[0];var minute=time[1];timeField.value=stunde+""+minute;autoCompleteTime_de(timeField);return;}}}
timeField.value=newtime;}
function addZIndex(){jQuery("#divMainHeader").addClass("divMainHeaderZIndex");}
function removeZIndex(){jQuery("#divMainHeader").removeClass("divMainHeaderZIndex");recalcMaxHeightForModalPanel();}
function recalcMaxHeightForModalPanel(){jQuery(".editPanel").css("max-height",maxHeightForModalPanels());jQuery(".viewPanel").css("max-height",maxHeightForModalPanels());}
function maxHeightForModalPanels(index){return(jQuery(document).height()-100)+"px";}
function toggleRemarkTextView(toggleItem,hideClass){toogleJElemet(jQuery(toggleItem),"down");toogleJElemet(jQuery(hideClass,toggleItem.parentNode),"hide");toogleJElemet(jQuery(".remarksMoreCount",toggleItem.parentNode),"hide");}
function toogleJElemet(jElement,toggleClass){if(jElement.hasClass(toggleClass)){jElement.removeClass(toggleClass);}else{jElement.addClass(toggleClass);}}